package main

import "fmt"

func searchInsert(nums []int, target int) int {
	for i, v := range nums {
		if v >= target {
			return i
		}
	}
	return len(nums)
}

func main() {
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 5), "expecting: 2")
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 2), "expecting: 1")
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 7), "expecting: 4")
}
