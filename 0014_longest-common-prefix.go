package main

import (
	"fmt"
)

func longestCommonPrefix(strs []string) string {
	mlen := 0
	for _, a := range strs {
		// fmt.Println("word", a)

		alen := len(a)

		if alen == 0 {
			return ""
		}

		if mlen == 0 || alen < mlen {
			mlen = alen
		}
	}

	// fmt.Println(mlen)

	match := ""

	for i := 0; i < mlen; i++ {
		// fmt.Println("char ", i)
		var char uint8 = 0
		for j, str := range strs {
			// fmt.Println("word #", j, str)
			if j == 0 {
				char = str[i]
			} else {
				if str[i] != char {
					return match
				}
			}
		}
		match += fmt.Sprintf("%c", char)
		// fmt.Println("match", match)
	}
	return match
}

func main() {
	input1 := []string{"flower", "flow", "flight"}
	fmt.Println("res1", longestCommonPrefix(input1))
	input2 := []string{"dog", "racecar", "car"}
	fmt.Println("res2", longestCommonPrefix(input2))
	input3 := []string{"", "b"}
	fmt.Println("res3", longestCommonPrefix(input3))

}
