package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func invertTree(root *TreeNode) *TreeNode {
	if root != nil {
		root.Left, root.Right = invertTree(root.Right), invertTree(root.Left)
	}
	return root
}

func flattenTreeNode(root *TreeNode) string {
	var treeMap map[int][]int = make(map[int][]int)
	var constructMap func(root *TreeNode, depth int)

	constructMap = func(root *TreeNode, depth int) {
		if root == nil {
			return
		}

		if treeMap[depth] == nil {
			treeMap[depth] = make([]int, 0, 0)
		}
		treeMap[depth] = append(treeMap[depth], root.Val)
		constructMap(root.Left, depth+1)
		constructMap(root.Right, depth+1)
	}

	constructMap(root, 0)

	r := make([]int, 0, 0)
	// for _, row := range treeMap {
	for i := 0; i < len(treeMap); i++ {
		for _, n := range treeMap[i] {
			r = append(r, n)
		}
	}
	return fmt.Sprint(r)
}

// copy pasta https://www.golangprograms.com/golang-program-to-implement-binary-tree.html
func (n *TreeNode) insert(Val int) {
	if n == nil {
		return
	} else if Val <= n.Val {
		if n.Left == nil {
			n.Left = &TreeNode{Val: Val, Left: nil, Right: nil}
		} else {
			n.Left.insert(Val)
		}
	} else {
		if n.Right == nil {
			n.Right = &TreeNode{Val: Val, Left: nil, Right: nil}
		} else {
			n.Right.insert(Val)
		}
	}
}

func constructTree(nums []int) TreeNode {
	if len(nums) == 0 {
		return TreeNode{}
	}
	tree := TreeNode{Val: nums[0]}
	nums = nums[1:]
	for _, n := range nums {
		tree.insert(n)
	}
	return tree
}

func main() {
	/*
		in := TreeNode{
			Val: 4,
			Left: &TreeNode{
				Val: 2,
				Left: &TreeNode{
					Val: 1,
				},
				Right: &TreeNode{
					Val: 3,
				},
			},
			Right: &TreeNode{
				Val: 7,
				Left: &TreeNode{
					Val: 6,
				},
				Right: &TreeNode{
					Val: 9,
				},
			},
		}
	*/
	in := constructTree([]int{4, 2, 7, 1, 3, 6, 9})
	fmt.Println(flattenTreeNode(&in), "->", flattenTreeNode(invertTree(&in)), "expecting: [4,7,2,9,6,3,1]")
}
