package main

import "fmt"

type PAIR = int8

const (
	PARENTHESIS  PAIR = iota
	CURLY_BRACE       = iota
	SQUARE_BRACE      = iota
)

type Pair struct {
	open  rune
	close rune
}

var PAIRS = map[PAIR]Pair{
	PARENTHESIS:  Pair{open: '(', close: ')'},
	CURLY_BRACE:  Pair{open: '{', close: '}'},
	SQUARE_BRACE: Pair{open: '[', close: ']'},
}

func isValid(s string) bool {
	stack := []PAIR{}
	for _, char := range s {
		for pairType, pair := range PAIRS {
			if char == pair.open {
				stack = append(stack, pairType)
			} else if char == pair.close {
				l := len(stack)
				if l == 0 {
					// there are no open brackets in the stack
					return false
				}
				if stack[l-1] != pairType {
					// last opened bracket is not the same type ( e.g [(] )
					return false
				}
				stack = stack[:l-1]
			}
		}
	}
	return len(stack) == 0
}

func main() {
	fmt.Println(PARENTHESIS, CURLY_BRACE, SQUARE_BRACE)
	fmt.Println("() should be true", isValid("()"))
	fmt.Println("()[]{} should be true", isValid("()[]{}"))
	fmt.Println("(] should be false", isValid("(]"))
	fmt.Println("([)] should be false ", isValid("(]"))
	fmt.Println("{[]} should be true ", isValid("{[]}"))

}
